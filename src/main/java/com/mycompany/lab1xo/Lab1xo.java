/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab1xo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class Lab1xo {

    private int board_size = 3;
    private List<List<String>> board;
    private String currentPlayer = "X";
    private boolean gameOver = false;

    public void newBoard() {
        board = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            List<String> row = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                row.add("-");
            }
            board.add(row);

        }
    }

    public void printBoard() {
        System.out.println("-------------");
        for (List<String> row : board) {
            System.out.print("| ");
            for (String cell : row) {
                System.out.print(cell + " | ");
            }
            System.out.println();
        }
        System.out.println("-------------");
    }

    public void play() {
        Scanner kb = new Scanner(System.in);
        newBoard();
        while (!gameOver) {
            printBoard();
            System.out.print(currentPlayer + " turn."+" Please input row[1-3] and column[1-3] : ");
            int row = kb.nextInt() - 1;
            int col = kb.nextInt() - 1;

            if (isMove(row, col)) {
                board.get(row).set(col, currentPlayer);
                if (checkWin(row, col)) {
                    printBoard();
                    System.out.println("Congratulations! Player " + currentPlayer + " wins!");
                    gameOver = true;

                } else if (checkDraw()) {
                    printBoard();
                    System.out.println("Draw!");
                    gameOver = true;
                } else {
                    currentPlayer = (currentPlayer.equals("X")) ? "O" : "X";
                }

            } else {
                System.out.println("Invalid move. Try again.");
            }
        }
        System.out.print("Do you want to play again? (y/n): ");
        String newGame = kb.next();
        if (newGame.equals("y")) {
            gameOver = false;
            play();
        } else {
            System.out.println("Thank you for playing! Goodbye!");
        }
        kb.close();
    }

    private boolean isMove(int row, int col) {
        if (row < 0 || row >= 3 || col < 0 || col >= 3) {
            return false;
        }
        return board.get(row).get(col).equals("-");
    }

    private boolean checkWin(int row, int col) {
        String player = board.get(row).get(col);

        // Check row
        boolean win = true;
        for (int c = 0; c < board_size; c++) {
            if (!board.get(row).get(c).equals(player)) {
                win = false;
                break;
            }
        }
        if (win) {
            return true;
        }

        // Check column
        win = true;
        for (int r = 0; r < board_size; r++) {
            if (!board.get(r).get(col).equals(player)) {
                win = false;
                break;
            }
        }
        if (win) {
            return true;
        }

        // Check diagonal
        win = true;
        if (row == col) {
            for (int i = 0; i < board_size; i++) {
                if (!board.get(i).get(i).equals(player)) {
                    win = false;
                    break;
                }
            }
            if (win) {
                return true;
            }
        }

        // Check anti-diagonal
        win = true;
        if (row + col == board_size - 1) {
            for (int i = 0; i < board_size; i++) {
                if (!board.get(i).get(board_size - 1 - i).equals(player)) {
                    win = false;
                    break;
                }
            }
            if (win) {
                return true;
            }
        }

        return false;
    }

    private boolean checkDraw() {
        for (List<String> row : board) {
            if (row.contains("-")) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("Welcome to ox program");
        Lab1xo game = new Lab1xo();
        game.play();

    }
}
